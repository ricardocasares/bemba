import styled from "@emotion/styled";

export const GhostButton = styled.button`
  text-align: left;
  border: none;
  background: transparent;
  color: var(--foreground);
  padding: 0;
  margin: 0;
  line-height: 0;
  display: block;
  width: 100%;
  font-size: var(--sz4);
`;
