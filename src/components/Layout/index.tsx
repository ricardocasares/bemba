import styled from "@emotion/styled";

export const Layout = styled.div`
  width: 100%;
  height: 100%;
  display: grid;
  position: fixed;
  grid-template-rows: auto 1fr auto;
`;
