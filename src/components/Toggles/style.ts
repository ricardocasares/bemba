import styled from "@emotion/styled";

export const ToggleItem = styled.div`
  display: grid;
  text-transform: capitalize;
  grid-template-columns: 1fr auto;
  align-items: center;
`;
