import styled from "@emotion/styled";

export const Scrollable = styled.div`
  overflow-y: scroll;
`;
